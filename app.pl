#!/usr/bin/env perl

package command::deploy;

use Mojo::Base 'Mojolicious::Command';

has description => 'Deploy database schema';
has usage       => "Usage: APPLICATION deploy\n";

sub run {
    my ($self) = @_;

    my $schema = Myriad::Schema->connect(@{ $self->app->config->{'db'} });

    $schema->deploy;
    $schema->resultset('Tracker')->create({
        host  => $self->app->config('tracker'),
        title => $self->app->config('tracker'),
    });
}

package main;

use Mojolicious::Lite;

push @{app->commands->namespaces}, 'command';

plugin 'Config' => { default => {
    db => [
        $ENV{MYRIAD_DSN},
        $ENV{MYRIAD_USER} ? (
            $ENV{MYRIAD_USER},
            $ENV{MYRIAD_PASSWORD}
        ) : (),
    ],
    tracker => $ENV{MYRIAD_TRACKER},
}};
plugin 'Myriad' => app->config;

helper 'byteformat' => sub {
    my ($c, $bytes) = @_;
    my %units = (
        1024 ** 4 => 'TiB',
        1024 ** 3 => 'GiB',
        1024 ** 2 => 'MiB',
        1024      => 'KiB',
    );

    for(sort { $b <=> $a } keys(%units)) {
        return sprintf("%.2f %s", $bytes / $_, $units{$_}) if $bytes >= $_;
    }
    return sprintf("%d B", $bytes);
};

helper 'chunks' => sub {
    my ($c, $string, $chunksize) = @_;
    return join(" ", unpack("(A${chunksize})*", $string));
};

get '/' => sub {
    my ($c) = @_;
    $c->stash->{'torrents'}       = Mojo::Collection->new($c->myriad->torrents->active->ascending('created'));
    $c->stash->{'totalcompletes'} = $c->myriad->torrents->num_completes;
    $c->stash->{'totaltransfer'}  = $c->myriad->torrents->transferred_bytes;
    $c->stash->{'swarmspeed'}     = $c->myriad->peers->active->swarm_speed;
} => 'index';

get '/:infohash' => [infohash => qr/[a-f0-9]{40}/] => sub {
    my ($c) = @_;

    my $torrent = $c->myriad->torrents->find({
        info_hash => pack('H*', $c->param('infohash'))
    });

    if(not $torrent) {
        $c->render('not_found');
    }

    $c->stash->{'torrent'}  = $torrent;
    $c->stash->{'seeders'}  = Mojo::Collection->new($torrent->peers->active->complete);
    $c->stash->{'leechers'} = Mojo::Collection->new($torrent->peers->active->incomplete->ascending('remaining'));
} => 'details';

app->start;
