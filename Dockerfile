FROM perl:5.24
COPY . /app
WORKDIR /app
RUN cpanm Carton
RUN carton install
CMD carton exec -- ./app.pl prefork -m production -l http://*:80 -w 2
