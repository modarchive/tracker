requires 'Myriad', '0.1.0' =>
    url => 'https://github.com/jantore/myriad/archive/v0.1.0.tar.gz';
requires 'Mojolicious::Plugin::Myriad', '0.0.4',
    url => 'https://github.com/jantore/mojolicious-plugin-myriad/archive/v0.0.4.tar.gz';

requires 'SQL::Translator', '>= 0.11018';

feature 'postgres', 'PostgreSQL support' => sub {
    requires 'DBD::Pg';
};

feature 'mysql', 'MySQL support' => sub {
    requires 'DBD::mysql';
};

feature 'sqlite', 'SQLite support' => sub {
    requires 'DBD::SQLite';
};
